import numpy as np
   

def meshgrid_index(*xi,indexing='ij'):
    """
    Create concatenated (contiguous in memory) flattened coordinate matrices from coordinate vectors.

    Parameters
    ----------
    *xi : 3D array_like
        Nx2xM array of vectors with starts and stops values where N 
        is the meshgrid dimension and M the number of vectors
    indexing : str, optional
        The coordinates indexing. The default is 'ij'.    
    
    Returns
    -------
    index : tuple
        The coordinates.
    split_index : ndarray
        The vectorization splitter array for index.

    """

    dtype=np.int64
    ndim=len(xi)
    
    #indexing option not activated, we force ij
    indexing='ij'
    if indexing not in ['xy', 'ij']:
        raise ValueError(
            "Valid values for `indexing` are 'xy' and 'ij'.")
    
    #working variables: numpy arrays            
    xi=np.asanyarray(xi,dtype=dtype)
        
    a_index      = [None]*ndim
    a_size       = [None]*ndim
    
    #build concatenated arrays range according to input starts and stops
    #for each dimension
    for r,a_range in enumerate(tuple(xi)):
        a_index[r],a_size[r]=arange(*tuple(a_range),return_lengths=True)  

    #output variables initialization
    index        = [None]*ndim
    split_index  = np.asanyarray(a_size,dtype=dtype).prod(axis=0) 
    
    #loop over meshgrid dimensions: use the vectorized version of tile and repeat to map indices
    for n in np.arange(ndim):
        
        #initialization
        nindex=a_index[n].copy()
        nsize=a_size[n].copy()

        #map towards left indices
        for n_b in np.arange(n)[::-1]:
            nindex=tile(nindex,nsize,a_size[n_b])
            nsize*=a_size[n_b]
            
        #map towards right indices
        for n_f in np.arange(n+1,ndim):  
            nindex=repeat(nindex,nsize,a_size[n_f])
            nsize*=a_size[n_f]
        
        #save index array for the current dimension
        index[n]=nindex
    
    #tuples are better than lists ...
    index=tuple(index)
    
    return index,split_index
    

def tile(a,size_a,reps):
    """
    Create contatenated tile for concatenated 1D vectors.

    Parameters
    ----------
    a : 1D array_like
        The input array.
    size_a : 1D array_like
        The vectorization splitter array for a.
    reps : 1D array_like
        The number of repetitions for each element.

    Returns
    -------
    1D array_like
        Vectorized array of tiled values.

    """
    
    a=np.asanyarray(a)
    size_a=np.asanyarray(size_a)
    reps=np.asanyarray(reps)
    
    size_v=size_a*reps
    size_t=size_v.sum()
    
    index=np.arange(size_t)
    
    shift=np.concatenate(((0,),np.cumsum(size_v-size_a)[:-1]))
    shift=np.repeat(shift,size_v)
    
    index-=shift
    
    shift=np.arange(reps.sum())-np.repeat(reps.cumsum()-reps,reps)
    shift*=np.repeat(size_a,reps)
    shift=np.repeat(shift,np.repeat(size_a,reps))
    
    index-=shift
    
    return a[index]


def repeat(a,size_a,reps):
    """
    Create contatenated repeat for concatenated 1D vectors.


    Parameters
    ----------
    a : 1D array_like
        The input array.
    size_a : 1D array_like
        The vectorization splitter array for a.
    reps : 1D array_like
        The number of repetitions for each element.

    Returns
    -------
    1D array_like
        Vectorized array of repeated values.

    """  

    a=np.asanyarray(a)
    size_a=np.asanyarray(size_a)
    reps=np.asanyarray(reps)
    
    return np.repeat(a,np.repeat(reps,size_a))



def arange(starts, stops, return_lengths=False):
    """
    Create concatenated ranges of integers for multiple start/stop
    Copyrights: https://codereview.stackexchange.com/questions/83018/vectorized-numpy-version-of-arange-with-multiple-start-stop
    

    Parameters
    ----------
    starts : 1D array_like
        Starts for each range.
    stops : 1D array_like
        Stops for each range (same shape as starts).
    return_lengths : bool, optional
        If activated, returns the vectorization splitter array. The default is False.

    Returns
    -------
    1D numpy.ndarray
        Vectorized array of evenly spaced values.

    Returns:
    --------
        numpy.ndarray: concatenated ranges

    For example:

        >>> starts = [1, 3, 4, 6]
        >>> stops  = [1, 5, 7, 6]
        >>> arange(starts, stops)
        array([3, 4, 4, 5, 6])
        
    """
    
    starts = np.asanyarray(starts)
    stops = np.asanyarray(stops)
    lengths = stops - starts
    crange = np.repeat(stops - lengths.cumsum(), lengths) + np.arange(lengths.sum())
    
    if return_lengths:
        return crange,lengths
    else:
        return crange
    
    
def norm(v):
    """ """
    return np.sqrt(np.sum(v**2,axis=-1))


def normalize(v):
    """ """
    return v/norm(v)[...,np.newaxis]


def orientate(v,epsilon=1E-3):
    """ """
    #random vector not colinear to all vectors
    vrdFound=False
    while(not vrdFound):
        vrd=normalize(np.random.rand(v.shape[-1]))
        vrdFound=np.sum(np.cross(vrd,v,axis=-1)**2,axis=-1) > epsilon
        vrdFound=vrdFound.all()
    
    #negative dot products are misoriented vectors
    v[np.sum(vrd[np.newaxis,...]*v,axis=-1)<0,...]*=-1